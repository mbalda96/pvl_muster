﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Aufgabe_1
{
    class Program
    {
        // Schleifenvariable für Hauptschleife
        private static bool _working = true;

        // Enum für mögliche Programmzustände
        private enum Mode{ Array, Häufigkeit, Ende, Fehler }

        // Enum für Zahlenspeicher der Array Routine
        private enum ZahlenGruppe { Gerade, Ungerade, Primzahl, Quadratzahl}
        static void Main(string[] args)
        {
            // wiederhole Programm bis es aktiv beendet wird
            while (_working)
            {
                // Zeige ein Menü und erzeuge einen Switch über dem Rückgabewert
                switch (ShowMenu())
                {
                    case Mode.Array:
                        ShowArray();
                        break;
                    case Mode.Häufigkeit:
                        ShowHäufigkeiten();
                        break;
                    case Mode.Ende:
                        _working = false;
                        Console.WriteLine("Programm wird beendet.");
                        break;

                    default: // alle andern Fälle werden als Fehler erkannt.
                        Console.WriteLine("Keine gültige Eingabe erkannt, bitte wiederholen sie den Vorgang.");
                        break;
                }
            }
            
        }

        private static void ShowHäufigkeiten()
        {
            Console.WriteLine("Bitte geben sie einen beliebigen String ein:\r\n");

            // Erzeuge ein neues Dictionary um die einzelnen Chars zu verwalten
            Dictionary<char, long> CharCounter = new Dictionary<char, long>();

            // Lese eine Zeile
            var input = Console.ReadLine();

            //Erzeuge zwei Leerzeilen
            Console.WriteLine("\r\n \r\n");

            // Prüfe ob Zeichen eingegeben wurden
            if (String.IsNullOrEmpty(input))
            {
                Console.WriteLine("Es wurden keine Zeichen eingegeben\r\n");
                return;
            }

            // Sortiere die Zeile anhand des Char Wertes
            // Orderby(i => i) nennt man Lambda Ausdruck
            var ordered_string = input.OrderBy(c => c);

            // iteriere über alle Chars des Strings
            foreach (char c in ordered_string)
            {
                // Prüfe ob Char bereits entdeckt wurde
                if (CharCounter.ContainsKey(c))
                {
                    // wurde bereits gefunden, also wird lediglich der Counter erhöht
                    CharCounter[c] += 1;
                }

                else
                {
                    // lege einen neuen Eintrag an
                    CharCounter.Add(c, 1);
                }
            }

            // Ausgabe des Dictionary

            foreach (var eintrag in CharCounter)
            {
                Console.WriteLine("\"{0}\", wurde {1}-mal eingegeben", eintrag.Key, eintrag.Value);
            }

        }

        private static void ShowArray()
        {
            // ZahlSpeicher ist ein Dictionary aus einer Zahlengruppe und einer Liste
            var ZahlSpeicher = new Dictionary<ZahlenGruppe, List<int>>()
            {
                // initialisiere das Dictionary mit allen möglichen Gruppen
                {ZahlenGruppe.Gerade, new List<int>()},
                {ZahlenGruppe.Ungerade, new List<int>()},
                {ZahlenGruppe.Primzahl, new List<int>()},
                {ZahlenGruppe.Quadratzahl, new List<int>()}
            };

            bool _fertig = false;

            while(!_fertig)
            {

                Console.WriteLine(
                    "Bitte geben sie eine Reihe von Zahlen an die durch Leerzeichen oder Komma getrennt sind:\r\n");

                // Lese eine Zeile
                var input = Console.ReadLine();

                //Erzeuge zwei Leerzeilen
                Console.WriteLine("\r\n \r\n");

                // Prüfe ob Zeichen eingegeben wurden
                if (String.IsNullOrEmpty(input))
                {
                    Console.WriteLine("Es wurden keine Zeichen eingegeben\r\n");
                    return;
                }

                // Array mit erlaubten Trennzeichen
                var _separators = new char[] {' ', ','};

                // Teile String an allen angegeben Trennzeichen in Teilstrings
                var _splitString = input.Split(_separators);

                // iteriere über alle Teilstrings
                foreach (string _s in _splitString)
                {
                    try
                    {
                        // Versuche den Teilstring in eine Zahl umzuwandeln
                        int number = Int32.Parse(_s);
                        if (IstGeradeZahl(number))
                        {
                            // Füge die nummer zur Liste der jeweiligen Kategorie hinzu
                            ZahlSpeicher[ZahlenGruppe.Gerade].Add(number);
                        }
                        else
                        {
                            ZahlSpeicher[ZahlenGruppe.Ungerade].Add(number);
                        }

                        if (IstQuadratZahl(number))
                        {
                            ZahlSpeicher[ZahlenGruppe.Quadratzahl].Add(number);
                        }

                        if (IstPrimZahl(number))
                        {
                            ZahlSpeicher[ZahlenGruppe.Primzahl].Add(number);
                        }
                    }
                    catch (Exception e)
                    {
                        // Fange mögliche Fehler ab
                        Console.WriteLine("String konnte nicht in eine Ganzzahl umgewandelt werden.\r\n " +
                                          "Bitte überprüfen sie ihre Eingabe!");
                    }
                }

                Ausgabe(ZahlSpeicher);

                Console.WriteLine("Wollen sie weitere Zahlen eingeben?");
                Console.WriteLine("j -- Weiter Zahlen eingeben");
                Console.WriteLine("n -- Routinenende");

                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.J:
                        break;
                    case ConsoleKey.N:
                        _fertig = true;
                        break;
                }
            }

        }
        /// <summary>
        /// Hier gehört die sortierte Ausgabe des Dictionary hin.
        /// Es muss über das Dictionary iteriert werden,
        /// Für jeden Key muss die Zugehörige Liste sortiert und ausgegeben werden
        /// Zum schluss muss geprüft werden ob alle Teillisten Einträge enthalten
        /// </summary>
        /// <param name="zahlSpeicher"></param>
        private static void Ausgabe(Dictionary<ZahlenGruppe, List<int>> zahlSpeicher)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Prüfe ob eingegebene Zahl eine Primzahl ist
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        private static bool IstPrimZahl(int number)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Prüfe ob eingegebene Zahl eine Quadratzahl ist
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        private static bool IstQuadratZahl(int number)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Prüfe ob eingegeben Zahl gerade ist
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        private static bool IstGeradeZahl(int number)
        {
            // Modulo 2 entpricht dem Rest nach der Division durch 2.
            // Gerade Zahlen haben keinen Rest
            return number % 2 == 0;
        }

        /// <summary>
        /// Zeigt ein Menü welches zur Programmauswahl verwendet wird
        /// </summary>
        /// <returns>Ein enum mit dem ausgewähltem Modus</returns>
        private static Mode ShowMenu()
        {
            Console.WriteLine("Bitte wählen sie ein Programm aus:\r\n");
            Console.WriteLine("1 - Array");
            Console.WriteLine("2 - Häufigkeit");
            Console.WriteLine("3 - Programm beenden");
            // lese eine eingegebene Taste
            // intercept verhindert das die eingegebene Taste angezeigt wird
            var _readKey = Console.ReadKey(true);
            // switch über eingegebene Tasten
            switch (_readKey.Key)
            {
                case ConsoleKey.D1:
                    return Mode.Array; 
                case ConsoleKey.D2:
                    return Mode.Häufigkeit;
                case ConsoleKey.D3:
                    return Mode.Ende;
                default:
                    return Mode.Fehler;
            }

        }
    }
}
